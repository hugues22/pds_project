package TP2;

import com.sun.org.apache.bcel.internal.generic.LALOAD;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;

import java.lang.reflect.Array;
import java.util.*;

// This file contains a simple LLVM IR representation
// and methods to generate its string representation

public class Llvm {
    static public class IR {
        List<Instruction> header; // IR instructions to be placed before the code (global definitions)
        List<Instruction> code;   // main code

        public IR(List<Instruction> header, List<Instruction> code) {
            this.header = header;
            this.code = code;
        }

        // append an other IR
        public IR append(IR other) {
            header.addAll(other.header);
            code.addAll(other.code);
            return this;
        }

        // append a code instruction
        public IR appendCode(Instruction inst) {
            code.add(inst);
            return this;
        }

        // append a code header
        public IR appendHeader(Instruction inst) {
            header.add(inst);
            return this;
        }

        // Final string generation
        public String toString() {
            // This header describe to LLVM the target
            // and declare the external function printf
            StringBuilder r = new StringBuilder("; Target\n" +
                    "target triple = \"x86_64-unknown-linux-gnu\"\n" +
                    "; External declaration of the printf function\n" +
                    "declare i32 @printf(i8* noalias nocapture, ...)\n" +
                    "declare i32 @scanf(i8* noalias nocapture, ...)\n\n");

            for(Instruction inst: header)
                r.append(inst);

            r.append("\n\n");

            // We create the function main
            // TODO : remove this when you extend the language
            //r.append("define i32 @main() {\n");


            for(Instruction inst: code)
                r.append(inst);

            // TODO : remove this when you extend the language
            // r.append("}\n");

            return r.toString();
        }
    }

    // Returns a new empty list of instruction, handy
    static public List<Instruction> empty() {
        return new ArrayList<Instruction>();
    }


    // LLVM Types
    static public abstract class Type {
        public abstract String toString();
    }

    static public class Int extends Type {
        public String toString() {
            return "i32";
        }
    }

    static public class StringValue extends Type {
    	String value;
        public String toString() {
            return "\""+ value  + "\"";
        }
    }

    // TODO : other types

    // LLVM IR Instructions
    static public abstract class Instruction {
        public abstract String toString();
    }

    static public class Add extends Instruction {
        Type type;
        String left;
        String right;
        String lvalue;

        public Add(Type type, String left, String right, String lvalue) {
            this.type = type;
            this.left = left;
            this.right = right;
            this.lvalue = lvalue;
        }

        public String toString() {
            return lvalue + " = add " + type + " " + left + ", " + right +  "\n";
        }
    }

    static public class Minus extends Instruction {
        Type type;
        String left;
        String right;
        String lvalue;

        public Minus(Type type, String left, String right, String lvalue) {
            this.type = type;
            this.left = left;
            this.right = right;
            this.lvalue = lvalue;
        }

        public String toString() {
            return lvalue + " = sub " + type + " " + left + ", " + right +  "\n";
        }
    }

    static public class Times extends Instruction {
        Type type;
        String left;
        String right;
        String lvalue;

        public Times(Type type, String left, String right, String lvalue) {
            this.type = type;
            this.left = left;
            this.right = right;
            this.lvalue = lvalue;
        }

        public String toString() {
            return lvalue + " = mul " + type + " " + left + ", " + right +  "\n";
        }
    }

    static public class Divide extends Instruction {
        Type type;
        String left;
        String right;
        String lvalue;

        public Divide(Type type, String left, String right, String lvalue) {
            this.type = type;
            this.left = left;
            this.right = right;
            this.lvalue = lvalue;
        }

        public String toString() {
            return lvalue + " = udiv " + type + " " + left + ", " + right +  "\n";
        }
    }


    
    static public class Bloc extends Instruction{
    	String name;
    	
    	public Bloc(String name) {
    		this.name = name;
    	}
    	
    	public String toString() {
    		return  name+":\n";
    	}
    }
    
    static public abstract class FinBloc extends Instruction {}
    
    //br label
    static public class Br extends FinBloc {
    	Label label;

    	public Br(String name){ this.label = new Label(name); }

    	public String toString() {
    		return "br " + label+"\n";
    	}
    }
    
    //br i1 cond label label
    static public class BrCond extends FinBloc {
    	
    	String cond;
    	Label label1;
    	Label label2;
    	
    	public BrCond(String cond, String label1, String label2) {
    		this.cond = cond;
    		this.label1 = new Label(label1);
    		this.label2 = new Label(label2);
    	}
    	
    	public String toString() {
    		return "br i1 " + cond + ", " + label1 + ", " + label2+"\n";
    	}
    }
    
    static public class Label extends Instruction{
    	Variable name;
    	
    	public Label(String name) {
    		this.name = new Variable(name);
    	}
    	
    	public String toString() {
    		return "label "+name;
    	}
    }

    static public class Icmp extends Instruction{
        Type type;
        String icmpValue;
        String lvalue;
        String value;

        public Icmp(Type type, String icmpValue, String lvalue, String value){
            this.type = type;
            this.icmpValue = icmpValue;
            this.lvalue = lvalue;
            this.value = value;
        }

        public String toString(){
            return icmpValue+" = icmp ne "+type+" "+lvalue+", "+value+"\n";
        }

    }

    static public class Affectation extends Instruction {
        Type exprType;
        Type varType;
        String expr;
        Variable name;

        public Affectation(Type exprType, Type varType, String expr, Variable name) {
            this.exprType = exprType;
            this.varType = varType;
            this.expr = expr;
            this.name = name;
        }

        public String toString() {
            return "store "+ exprType + " " + expr+ ", "+ varType + " "+ name+  "\n";
        }
    }

    static public class DeclarationVariable extends Instruction{

        Type type;
        Variable name;

        public DeclarationVariable(Type type, Variable name){
            this.type = new Int();
            this.name = name;
        }

        public String toString() {
            return name + " = alloca " + type + " \n";
        }
    }

    static public class DefineFunction extends Instruction{

        Type type;
        String name;
        ArrayList<SymbolTable.VariableSymbol> params;
        //HashMap<SymbolTable.VariableSymbol, SymbolTable.VariableSymbol> paramsCorrespondance;
        
        public DefineFunction(Type type, String name, ArrayList<SymbolTable.VariableSymbol> params) {
        	this.type = type;
        	this.name = name; 
        	this.params = params;
        	//this.paramsCorrespondance = paramsCorrespondance;
        }

        public String toString() {
            String ret =  "define " + type +" @"+name+"(";
            for(int i = 0; i < params.size() ; i++) {
            	ret += params.get(i).toLlvm();
            	if(i < params.size()-1){
            		ret += ", ";
            	}
            }
            ret += "){\n";

            for(SymbolTable.VariableSymbol sym : params){
                ret += new DeclarationVariable(sym.type.toLlvmType(), new Variable(sym.ident+ASD.Line.RANK)).toString();
                ret += new Affectation(sym.type.toLlvmType(), new IntVar(), "%"+sym.ident, new Variable(sym.ident+ASD.Line.RANK));
            }
            return ret;
        }
    }
    
    static public class EndFunction extends Instruction{
    	Type type;
    	
    	public EndFunction(Type type) {
    		this.type = type;
    	}
    	
    	public String toString() {
    		
    		return (type.toString().equals("void")? "ret void\n" : "ret i32 0\n")+"}\n\n";
    	}
    }
    

    static public class ReturnVoid extends Instruction {
        public String toString() { return "ret void"; }
    }

    static public class Return extends Instruction {
        Type type;
        String value;

        public Return(Type type, String value) {
            this.type = type;
            this.value = value;
        }

        public String toString() {
            return "ret " + type + " " + value + "\n";
        }
    }

    static public class VariableExpression extends Instruction {
        Type type;
        Variable name;
        String lvalue;

        public VariableExpression(Type type, Variable name, String lvalue){
            this.type = type;
            this.name = name;
            this.lvalue = lvalue;
        }

        public String toString() {
            //return lvalue+" = load "+ type + " " + name+"\n";
            return lvalue+" = load i32, "+ type + " " + name+"\n";
        }
    }

    static public class FunctionInstruction extends Instruction{
        Type typeFunction;
        String nameFunction;
        ArrayList<Type> typeParams;
        ArrayList<String> nameParams;

        public FunctionInstruction(Type typeFunction, String nameFunction, ArrayList<Type> typeParams, ArrayList<String> nameParams){
            this.typeFunction = typeFunction;
            this.nameFunction = nameFunction;
            this.typeParams = typeParams;
            this.nameParams = nameParams;
        }

        public String toString(){
            String ret = "";
            for(int i = 0; i < typeParams.size(); i++) {
                ret += typeParams.get(i) + " " + nameParams.get(i);
                if(i < typeParams.size()-1) {
                    ret += ", ";
                }
            }
            return "call "+ typeFunction + " @" + nameFunction+"(" + ret + ")\n";
        }
    }

    static public class FunctionExpression extends Instruction {
        Type typeFunction;
        String nameFunction;
        ArrayList<Type> typeParams;
        ArrayList<String> nameParams;
        String lvalue;

        public FunctionExpression(Type typeFunction, String nameFunction, ArrayList<Type> typeParams, ArrayList<String> nameParams, String lvalue){
            this.typeFunction = typeFunction;
            this.nameFunction = nameFunction;
            this.typeParams = typeParams;
            this.nameParams = nameParams;
            this.lvalue = lvalue;
        }

        public String toString() {
        	String ret = "";
        	for(int i = 0; i < typeParams.size(); i++) {
        		ret += typeParams.get(i) + " " + nameParams.get(i);
        		if(i < typeParams.size()-1) {
        			ret += ", ";
        		}
        	}
            return lvalue+" = call "+ typeFunction + " @" + nameFunction+"(" + ret + ")\n";
        }
    }

    static public class Fmt extends Instruction{
    	String value;
    	String fmt; 
    	
    	public Fmt(String value, String fmt) {
    		this.value = value; 
    		this.fmt = fmt;
    	}
    	
    	public String toString() {
    		Utils.LLVMStringConstant valueFmt = Utils.stringTransform(value);
    		return fmt+" = global ["+ valueFmt.length+" x i8] c\""+valueFmt.str+"\"\n";
    	}
    }

    static public class Print extends Instruction{

        String value;
        String fmt;
        ArrayList<String> results;

        public Print(String value, String fmt, ArrayList<String> results){
            this.value = value;
            this.fmt = fmt;
            this.results = results;
        }

        public String toString(){
            String ret = "";
            if(results.size() > 1 && !results.get(0).equals("")) {
	            for(String result : results){
	                ret += ", i32 "+result;
	            }
            }
            Utils.LLVMStringConstant valueFmt = Utils.stringTransform(value);
            //use the good version
            //return "call i32 (i8*, ... )* @printf(i8* getelementptr inbounds (["+valueFmt.length+" x i8]* "+fmt+", i32 0, i32 0)"+ret+")\n";
            return "call i32 (i8*, ... ) @printf(i8* getelementptr inbounds (["+valueFmt.length+" x i8], ["+valueFmt.length+" x i8]* "+fmt+", i32 0, i32 0)"+ret+")\n";
        	
        }
    }

    static public class Scan extends Instruction{

        String fmt;
        Variable var;

        public Scan(String fmt, String var){
            this.fmt = fmt;
            this.var = new Variable(var);
        }

        public String toString(){
            return "call i32 (i8*, ... ) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* "+fmt+", i32 0, i32 0), i32* "+var+")\n";
        }
    }

    static public class Variable /*extends Instruction*/{
        String name;

        public Variable(String name){
            this.name = name;
        }

        public String toString() { return "%"+name; }
    }

    static public class IntVar extends Type {
        public String toString() {
            return "i32*";
        }
    }

    static public class Void extends Type {
        public String toString() {
            return "void";
        }
    }
}
