package TP2;

import com.sun.org.apache.bcel.internal.generic.INEG;
import com.sun.org.apache.bcel.internal.generic.ReturnInstruction;
import javafx.scene.chart.ValueAxis;
import jdk.nashorn.internal.ir.Symbol;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import javax.naming.ldap.ExtendedRequest;
import java.sql.Array;
import java.util.*;


public class ASD {

    static public class Program {

        // What a program contains.
        ArrayList<DeclFunction> declFunctions;
        SymbolTable table;
        public Program(ArrayList<DeclFunction> declFunctions) {
            this.table = new SymbolTable();
            this.declFunctions = declFunctions;
        }

        // Pretty-printer
        public String pp() {
            String pp = "";
            for(DeclFunction declFunction : declFunctions){
                pp += declFunction.pp()+"\n";
            }
            return pp;
        }

        // IR generation
        public Llvm.IR toIR() throws TypeException {

            Iterator<ASD.DeclFunction> it = declFunctions.iterator();

            boolean main = false;

            // computes the IR of the expression
            Instruction.RetExpression retExpr = new Instruction.RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");
            while(it.hasNext()) {
                DeclFunction function = it.next();
                if(!function.defined) {
                    if (function instanceof Proto) {
                        Iterator<DeclFunction> tmp = declFunctions.iterator();
                        while (tmp.hasNext()) {
                            DeclFunction functionTmp = tmp.next();
                            if (functionTmp.name.equals(function.name) && functionTmp instanceof Function) {
                                if(functionTmp.params.size() != function.params.size()){
                                    throw new TypeException("mismatch for function "+function.name+": not the same number of parameters");
                                }
                                if(!functionTmp.type.pp().equals(function.type.pp())){
                                    throw new TypeException("type mismatch for function "+function.name+": have " + function.type.pp()+ " for the proto and " + functionTmp.type.pp()+" for the declared function");
                                }
                                retExpr.ir.append(functionTmp.toIR(table).ir);
                                functionTmp.defined = true;
                            }
                        }
                    } else {
                        retExpr.ir.append(function.toIR(table).ir);
                        function.defined = true;
                    }
                    if(function.name.equals("main")) main = true;
                }
            }
            if(!main) {
                throw new TypeException("expected function main");
            }
            // add a return instruction
            return retExpr.ir;
        }
    }

    // All toIR methods returns the IR, plus extra information (synthesized attributes)
    // They can take extra arguments (inherited attributes)
    static public abstract class Line{

        static public int RANK = 0;
        public abstract String pp();

        public abstract RetExpression toIR(SymbolTable table) throws TypeException;
        // Object returned by toIR on expressions, with IR + synthesized attributes
        static public class RetExpression /*extends Instr.RetInstr */{

            // The LLVM IR:
            public Llvm.IR ir;
            // And additional stuff:
            public Type type; // The type of the expression
            public String result; // The name containing the expression's result
            // (either an identifier, or an immediate value)
            public RetExpression (Llvm.IR ir, Type type, String result) {
                this.ir = ir;
                this.type = type;
                this.result = result;
            }
        }
    }

    static public abstract class DeclFunction extends Line {

        ArrayList<SymbolTable.VariableSymbol> params;
        String name;
        Type type;
        Boolean defined = false;
    }

    static public class Proto extends DeclFunction{

        public Proto(String type, String name, ArrayList<SymbolTable.VariableSymbol> params){
            this.type = (type.equals("INT")?  new Int() : new Void());
            this.name = name;
            this.params = params;
        }

        public String pp() {
            String ret = "proto "+  type.pp() + " " +name+"(";
            for(SymbolTable.VariableSymbol sym : params){
                ret +=" "+sym.type.pp() + " "+sym.ident + " ";
            }
            return ret +")\n";
        }


        public RetExpression toIR(SymbolTable table) throws TypeException {
            table.add(new SymbolTable.FunctionSymbol(type, name, params, false));
            return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");
        }

    }
    static public class Function extends DeclFunction{

        Instruction instruction;

        public Function(String type, String name, ArrayList<SymbolTable.VariableSymbol> params, Instruction instruction) {
            this.type = (type.equals("INT")?  new Int() : new Void());
            this.name = name;
            this.params = params;
            this.instruction = instruction;
        }

        public String pp() {

            String ret = "FUNCTION "+  type.pp() + " " +name+"(";
            for(SymbolTable.VariableSymbol sym : params){
                ret +=" "+sym.type.pp() + " "+sym.ident + " ";
            }
            return ret +") "+instruction.pp()+" \n";
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {

            table.add(new SymbolTable.FunctionSymbol(type, name, params, true));
            table = new SymbolTable(table);

            for(SymbolTable.VariableSymbol sym : params) {
                table.add(sym);
                SymbolTable.VariableSymbol tmp = new SymbolTable.VariableSymbol(new IntVar(), sym.ident+Line.RANK);
                table.add(tmp);
            }

            RetExpression retInstruction = instruction.toIR(table);
            RetExpression ret = new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");

            Llvm.DefineFunction defFunc = new Llvm.DefineFunction(type.toLlvmType(), name, params);
            Llvm.EndFunction endFunc = new Llvm.EndFunction(type.toLlvmType());
            ret.ir.appendCode(defFunc);
            ret.ir.append(retInstruction.ir);
            ret.ir.appendCode(endFunc);
            return ret;
        }
    }

    static public class Print extends Instruction{

        String value;
        ArrayList<Expression> expressions;

        public Print(String value, ArrayList<Expression> expressions){
            this.value = value;
            this.expressions = expressions;
        }

        public String pp() {
            String ret = "";
            for(Expression expression : expressions){
                ret += expression.pp() + ",";
            }
            return "PRINT(\"" + value +"\", "+ ret +")\n";
        }


        public RetExpression toIR(SymbolTable table) throws TypeException {
            RetExpression ret = new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");
            ArrayList<String> results = new ArrayList<String>();

            for(Expression expression : expressions){
                RetExpression tmpRet = expression.toIR(table);
                ret.ir.append(tmpRet.ir);
                results.add(tmpRet.result);
            }

            String fmtResult = Utils.newglob("@.fmt");
            Llvm.Fmt fmt = new Llvm.Fmt(value, fmtResult);
            ret.ir.appendHeader(fmt);
            Llvm.Print print = new Llvm.Print(value, fmtResult, results);
            ret.ir.appendCode(print);
            return ret;
        }
    }

    static public class Scan extends Instruction{

        static String fmtScan = null;

        SymbolTable.Symbol sym;

        public Scan(SymbolTable.Symbol sym){
            this.sym = sym;
        }

        public String pp() {
            return "READ "+sym.ident;
        }


        public RetExpression toIR(SymbolTable table) throws TypeException {
            if(table.lookup(sym.ident+Line.RANK) == null ) throw new TypeException("variable \"" + sym.ident+ "\" doesn't exists");
            RetExpression ret = new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");
            if(Scan.fmtScan == null){
                Scan.fmtScan = Utils.newglob("@.fmt");
                Llvm.Fmt fmt = new Llvm.Fmt("%d", Scan.fmtScan);
                ret.ir.appendHeader(fmt);
            }
            String fmt = Scan.fmtScan;
            Llvm.Scan scan = new Llvm.Scan(fmt, sym.ident+Line.RANK);
            ret.ir.appendCode(scan);
            return ret;
        }
    }

    static public abstract class Instruction extends Line {}

    static public class Bloc extends Instruction{

        ArrayList<ASD.Instruction> instructions;
        String pp;

        public Bloc(ArrayList<ASD.Instruction> instructions) {

            this.instructions = instructions;
        }

        public String pp() {
            pp = "{ \n";
            instructions.forEach(instruction -> {
                pp += instruction.pp()+"\n";
            });
            pp += "}";
            return pp;
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {
            table = new SymbolTable(table);
            Iterator<ASD.Instruction> instr = instructions.iterator();
            if(instr.hasNext()) {
                Line.RANK++;
                RetExpression retExpression = instr.next().toIR(table);
                while (instr.hasNext()) {
                    retExpression.ir.append(instr.next().toIR(table).ir);
                }
                Line.RANK--;
                return retExpression;
            }
            return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");
        }
    }

    static public class While extends Instruction{

        Comparison comparison;
        Instruction instruction;

        public While(Comparison comparison, Instruction instruction) {
            this.comparison= comparison;
            this.instruction = instruction;
        }

        public String pp() {
            return "WHILE " + comparison.pp()+" DO \n"+instruction.pp()+"\nDONE";
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {
            RetExpression retComparison = comparison.toIR(table);
            RetExpression retInstruction = instruction.toIR(table);
            RetExpression ret = new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");

            String labelLoop =  Utils.newlab("while");
            String labelBody =  Utils.newlab("do");
            String labelAfter =  Utils.newlab("done");

            Llvm.Instruction brLoop = new Llvm.Br(labelLoop);
            Llvm.Instruction brCond = new Llvm.BrCond(retComparison.result, labelBody, labelAfter);

            Llvm.Instruction blocLoop = new Llvm.Bloc(labelLoop);
            Llvm.Instruction blocBody = new Llvm.Bloc(labelBody);
            Llvm.Instruction blocAfter = new Llvm.Bloc(labelAfter);
            

            ret.ir.appendCode(brLoop)
                    .appendCode(blocLoop).append(retComparison.ir).appendCode(brCond)
                    .appendCode(blocBody).append(retInstruction.ir).appendCode(brLoop)
                    .appendCode(blocAfter);
            return ret;
        }
    }

    static public abstract class Condition extends Instruction {}

    static public class IfThen extends Condition{

        Comparison comparison;
        Instruction instruction;

        public IfThen(Comparison comparison, Instruction instruction){
            this.comparison = comparison;
            this.instruction = instruction;
        }

        public String pp() {
            return "IF " + comparison.pp()+"\nTHEN \n"+instruction.pp()+" FI";
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {
            RetExpression retComparison = comparison.toIR(table);
            RetExpression retInstruction = instruction.toIR(table);

            String labelThen =  Utils.newlab("then");
            String labelFi =  Utils.newlab("fi");

            Llvm.Instruction brCond = new Llvm.BrCond(retComparison.result, labelThen, labelFi);
            Llvm.Instruction brFi = new Llvm.Br(labelFi);

            Llvm.Instruction blocThen = new Llvm.Bloc(labelThen);
            Llvm.Instruction blocFi = new Llvm.Bloc(labelFi);

            retComparison.ir.appendCode(brCond)
                    .appendCode(blocThen).append(retInstruction.ir).appendCode(brFi)
                    .appendCode(blocFi);
            return retComparison;
        }
    }

    static public class IfThenElse extends Condition{

        Comparison comparison;
        Instruction instructionThen;
        Instruction instructionElse;

        public IfThenElse(Comparison comparison, Instruction instructionThen, Instruction instructionElse){
            this.comparison = comparison;
            this.instructionThen = instructionThen;
            this.instructionElse = instructionElse;
        }

        public String pp() {
            return "IF "+ comparison.pp() + "\nTHEN \n" + instructionThen.pp() + " \nELSE\n" + instructionElse.pp()+"\nFI";
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {
            RetExpression retComparison = comparison.toIR(table);
            RetExpression retInstructionThen = instructionThen.toIR(table);
            RetExpression retInstructionElse = instructionElse.toIR(table);

            String labelThen =  Utils.newlab("then");
            String labelElse =  Utils.newlab("else");
            String labelFi =  Utils.newlab("fi");

            Llvm.Instruction brCond = new Llvm.BrCond(retComparison.result, labelThen, labelElse);
            Llvm.Instruction brFi = new Llvm.Br(labelFi);

            Llvm.Instruction blocThen = new Llvm.Bloc(labelThen);
            Llvm.Instruction blocElse = new Llvm.Bloc(labelElse);
            Llvm.Instruction blocFi = new Llvm.Bloc(labelFi);

            retComparison.ir.appendCode(brCond)
                    .appendCode(blocThen).append(retInstructionThen.ir).appendCode(brFi)
                    .appendCode(blocElse).append(retInstructionElse.ir).appendCode(brFi)
                    .appendCode(blocFi);

            return retComparison;
        }
    }

    static public class Comparison extends Line{

        Expression expression;

        public Comparison(Expression expression){
            this.expression = expression;
        }

        public String pp() {
            return expression.pp();
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {
            RetExpression retExpression = expression.toIR(table);

            String result = Utils.newlab("%icmp");
            Llvm.Instruction icmp = new Llvm.Icmp(retExpression.type.toLlvmType(), result, retExpression.result, "0");

            retExpression.ir.appendCode(icmp);
            retExpression.result = result;
            return retExpression;
        }
    }

    //Abstract class for expressions
    static public abstract class Expression extends Instruction{}

    // Concrete class for Expression: add case
    static public class AddExpression extends Expression {
        Expression left;
        Expression right;

        public AddExpression(Expression left, Expression right) {
            this.left = left;
            this.right = right;
        }

        // Pretty-printer
        public String pp() {
            return "(" + left.pp() + " + " + right.pp() + ")";
        }

        // IR generation
        public RetExpression toIR(SymbolTable table) throws TypeException {

            RetExpression leftRet = left.toIR(table);
            RetExpression rightRet = right.toIR(table);

            // We check if the types mismatches
            if(!leftRet.type.equals(rightRet.type)) {
                throw new TypeException("type mismatch: have " + leftRet.type + " and " + rightRet.type);
            }

            // We base our build on the left generated IR:
            // append right code
            leftRet.ir.append(rightRet.ir);

            // allocate a new identifier for the result
            String result = Utils.newlab("%plus");

            // new add instruction result = left + right
            Llvm.Instruction add = new Llvm.Add(leftRet.type.toLlvmType(), leftRet.result, rightRet.result, result);

            // append this instruction
            leftRet.ir.appendCode(add);

            // return the generated IR, plus the type of this expression
            // and where to find its result
            return new RetExpression(leftRet.ir, leftRet.type, result);
        }
    }
    //Concrete class for Expression: MINUS case
    static public class MinusExpression extends Expression {

        Expression left;
        Expression right;
        public MinusExpression(Expression left, Expression right) {
            this.left = left;
            this.right = right;
        }

        // Pretty-printer
        public String pp() {
            return "(" + left.pp() + " - " + right.pp() + ")";
        }
        // IR generation

        public RetExpression toIR(SymbolTable table) throws TypeException {

            RetExpression leftRet = left.toIR(table);
            RetExpression rightRet = right.toIR(table);

            // We check if the types mismatches
            if(!leftRet.type.equals(rightRet.type)) {
                throw new TypeException("type mismatch: have " + leftRet.type + " and " + rightRet.type);
            }

            // We base our build on the left generated IR:
            // append right code
            leftRet.ir.append(rightRet.ir);

            // allocate a new identifier for the result
            String result = Utils.newlab("%minus");

            // new mins instruction result = left - right
            Llvm.Instruction minus = new Llvm.Minus(leftRet.type.toLlvmType(), leftRet.result, rightRet.result, result);

            // append this instruction
            leftRet.ir.appendCode(minus);

            // return the generated IR, plus the type of this expression
            // and where to find its result
            return new RetExpression(leftRet.ir, leftRet.type, result);
        }
    }
    //Concrete class for Expression: TIMES case
    static public class TimesExpression extends Expression {

        Expression left;
        Expression right;
        public TimesExpression(Expression left, Expression right) {
            this.left = left;
            this.right = right;
        }

        // Pretty-printer

        public String pp() {
            return "(" + left.pp() + " * " + right.pp() + ")";
        }
        // IR generation

        public RetExpression toIR(SymbolTable table) throws TypeException {
            RetExpression leftRet = left.toIR(table);
            RetExpression rightRet = right.toIR(table);

            // We check if the types mismatches
            if(!leftRet.type.equals(rightRet.type)) {
                throw new TypeException("type mismatch: have " + leftRet.type + " and " + rightRet.type);
            }

            // We base our build on the left generated IR:
            // append right code
            leftRet.ir.append(rightRet.ir);

            // allocate a new identifier for the result
            String result = Utils.newlab("%times");

            // new mins instruction result = left * right
            Llvm.Instruction times = new Llvm.Times(leftRet.type.toLlvmType(), leftRet.result, rightRet.result, result);

            // append this instruction
            leftRet.ir.appendCode(times);

            // return the generated IR, plus the type of this expression
            // and where to find its result
            return new RetExpression(leftRet.ir, leftRet.type, result);
        }
    }
    //Concrete class for Expression: TIMES case
    static public class DivideExpression extends Expression {

        Expression left;
        Expression right;
        public DivideExpression(Expression left, Expression right) {
            this.left = left;
            this.right = right;
        }

        // Pretty-printer

        public String pp() {
            return "(" + left.pp() + " * " + right.pp() + ")";
        }
        // IR generation

        public RetExpression toIR(SymbolTable table) throws TypeException {
            RetExpression leftRet = left.toIR(table);
            RetExpression rightRet = right.toIR(table);

            // We check if the types mismatches
            if(!leftRet.type.equals(rightRet.type)) {
                throw new TypeException("type mismatch: have " + leftRet.type + " and " + rightRet.type);
            }

            // We base our build on the left generated IR:
            // append right code
            leftRet.ir.append(rightRet.ir);

            // allocate a new identifier for the result
            String result = Utils.newlab("%divide");

            // new mins instruction result = left / right
            Llvm.Instruction divide = new Llvm.Divide(leftRet.type.toLlvmType(), leftRet.result, rightRet.result, result);

            // append this instruction
            leftRet.ir.appendCode(divide);

            // return the generated IR, plus the type of this expression
            // and where to find its result
            return new RetExpression(leftRet.ir, leftRet.type, result);
        }
    }
    // Concrete class for Expression: constant (integer) case
    static public class IntegerExpression extends Expression {

        int value;
        public IntegerExpression(int value) {
            this.value = value;
        }
        public String pp() {
            return "" + value;
        }

        public RetExpression toIR(SymbolTable table) {
            // Here we simply return an empty IR
            // the `result' of this expression is the integer itself (as string)
            return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "" + value);
        }
    }

    static public class VariableExpression extends Expression {

        Variable variable;

        public VariableExpression(Variable variable){
            this.variable = variable;
        }

        public String pp() {
            return variable.name;
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {

            for(int i = Line.RANK; i >= 0; i--) {
                if (table.lookup(variable.name + i) != null) {
                    // allocate a new identifier for the result
                    Llvm.Variable left = variable.toLlvmType();
                    String result = Utils.newtmp();
                    Llvm.Instruction declaration = new Llvm.VariableExpression(variable.type.toLlvmType(), variable.toLlvmType(i), result);

                    return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()).appendCode(declaration), new Int(), result);
                }
            }
            throw new TypeException("variable \"" + variable.name+ "\" doesn't exists");
        }
    }

    static public class FunctionExpression extends Expression {

        String name;
        ArrayList<Expression> params;

        public FunctionExpression(String name, ArrayList<Expression> params){
            this.name = name;
            this.params = params;
        }

        public String pp() {
        	String ret = "";
        	for(int i = 0; i < params.size(); i++) {
        		ret += params.get(i).pp();
        		if(i < params.size()-1) ret += ", ";
        	}
            return this.name +"("+ret+")";
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {

            if(table.lookup(name) != null){

            	ArrayList<Llvm.Type> types = new ArrayList<>();
            	ArrayList<String> names = new ArrayList<>();
            	
            	RetExpression ret = new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");
            	
            	for(Expression expr : params) {
            		RetExpression retexpr = expr.toIR(table);
            		ret.ir.append(retexpr.ir);
            		types.add(retexpr.type.toLlvmType());
            		names.add(retexpr.result);
            	}

                String result = Utils.newlab("%ecall");
                Llvm.Instruction function = new Llvm.FunctionExpression(new Int().toLlvmType(), name, types, names, result);

                ret.ir.appendCode(function);
                return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()).append(ret.ir), new Int(), result);
            }
            else {
                throw new TypeException("function \"" + name + "\" doesn't exists");
            }
        }
    }

    static public class FunctionInstruction extends Instruction {

        String name;
        ArrayList<Expression> params;

        public FunctionInstruction(String name, ArrayList<Expression> params){
            this.name = name;
            this.params = params;
        }

        public String pp() {
            String ret = "";
            for(int i = 0; i < params.size(); i++) {
                ret += params.get(i).pp();
                if(i < params.size()-1) ret += ", ";
            }
            return this.name +"("+ret+")";
        }


        public RetExpression toIR(SymbolTable table) throws TypeException {

            SymbolTable.Symbol sym = table.lookup(name);

            if(table.lookup(name) != null){
                ArrayList<Llvm.Type> types = new ArrayList<>();
                ArrayList<String> names = new ArrayList<>();

                RetExpression ret = new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");

                for(Expression expr : params) {
                    RetExpression retexpr = expr.toIR(table);
                    ret.ir.append(retexpr.ir);
                    types.add(retexpr.type.toLlvmType());
                    names.add(retexpr.result);
                }

                    Llvm.Instruction function = new Llvm.FunctionInstruction(((SymbolTable.FunctionSymbol) sym).returnType.toLlvmType(), name, types, names);

                ret.ir.appendCode(function);
                return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()).append(ret.ir), new Int(), "");
            }
            else {
                throw new TypeException("function \"" + name + "\" doesn't exists");
            }
        }
    }

    static public class Affectation extends Instruction{ //TODO GERER LES RANGS

        Variable variable;
        Expression expression;

        public Affectation(Variable variable, Expression expression){
            this.variable = variable;
            this.expression = expression;

            variable.setExpression(expression);
        }

        public String pp() {
            return ("("+variable.pp()+":="+expression.pp()+")");
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {

            RetExpression expr = expression.toIR(table);

            for(int i = Line.RANK; i >= 0; i--) {
                if (table.lookup(variable.name + i) != null) {
                    Llvm.Instruction affectation = new Llvm.Affectation(expr.type.toLlvmType(), variable.type.toLlvmType(), expr.result, variable.toLlvmType(i));
                    expr.ir.appendCode(affectation);
                    return new RetExpression(expr.ir, new Int(), variable.name+i);
                }
            }
            throw new TypeException("variable " + variable.name + " doesn't exists");
        }
    }

    static public class Declaration extends Instruction{

        ArrayList<DeclElem> elements;
        Type type;

        public Declaration(Type type, ArrayList<DeclElem> elements){
            this.type = type;
            this.elements = elements;
        }

        public String pp() {
            String pp = type.pp() + " ";
            for(DeclElem element : elements){
                pp += element.pp() +" ";
            }
            return pp;
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {
            RetExpression ret = new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new Int(), "");
            for(DeclElem tmp : elements){
                if(table.lookup(tmp.getName()+Line.RANK) == null) {
                    ret.ir.append(tmp.toIR(table).ir);
                    table.add(new SymbolTable.VariableSymbol(tmp.getType(), tmp.getName()+Line.RANK));
                }
                else {
                    throw new TypeException("mutliple definition of "+ tmp.getName());
                }
            }
            return ret;
        }
    }

    static public class Return extends Instruction{

        Expression expression;
        String type;

        public Return(Expression expression, String type){
            this.expression = expression;
            this.type = type;
        }

        public String pp() {
            return "RETURN "+expression.pp();
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {
            RetExpression expr = expression.toIR(table);

            if(!type.equals("INT")) {
                throw new TypeException("type mismatch : function does not return an int");
            }
            Llvm.Return ret = new Llvm.Return(expr.type.toLlvmType(), expr.result);

            expr.ir.appendCode(ret);
            return expr;
        }
    }

    static public abstract class DeclElem extends Line{
        public abstract String getName();
        public abstract Type getType();
        public abstract void setName(String name);
    }

    static public class DeclVariable extends DeclElem {

        Variable variable;

        public DeclVariable(Variable variable){
            this.variable = variable;
        }

        @Override
        public String getName() {
            return variable.name;
        }

        @Override
        public Type getType() {
            return variable.type;
        }

        @Override
        public void setName(String name) {
            this.variable.name = name;
        }

        public String pp() {
            return variable.name;
        }

        public RetExpression toIR(SymbolTable table) throws TypeException {
            Llvm.Instruction declaration = new Llvm.DeclarationVariable(variable.type.toLlvmType(), variable.toLlvmType());
            return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()).appendCode(declaration), variable.type, "");
        }
    }

    // Warning: this is the type from VSL+, not the LLVM types!
    static public abstract class Type {

        public abstract String pp();
        public abstract Llvm.Type toLlvmType();
    }
    static class Int extends Type {

        public String pp() {
            return "INT";
        }

        @Override public boolean equals(Object obj) {
            return obj instanceof Int;
        }

        public Llvm.Type toLlvmType() {
            return new Llvm.Int();
        }
    }
    static class StringValue extends Type {

        public String pp() {
            return "STRING";
        }

        @Override public boolean equals(Object obj) {
            return obj instanceof StringValue;
        }

        public Llvm.Type toLlvmType() {
            return new Llvm.StringValue();
        }
    }

    static class Void extends Type{

        public String pp() {
            return "VOID";
        }

        public Llvm.Type toLlvmType() {
            return new Llvm.Void();
        }
    }


    static class IntVar extends Type {

        public String pp() {
            return "";
        }

        @Override public boolean equals(Object obj) {
            return obj instanceof IntVar;
        }

        public Llvm.Type toLlvmType() {
            return new Llvm.IntVar();
        }
    }


    static public class Variable{

        //Type of the variable
        Type type;
        //Name of the variable
        String name;
        //value of the variable
        Expression expression;

        public Variable(Type type, String name){
            this.type = type;
            this.name = name;
        }

        public Variable(String name, Expression expression){ this.name = name; this.expression = expression; }
        public void setExpression(Expression expression){ this.expression = expression; }
        public String pp() {return name; }
        public Llvm.Variable toLlvmType() { return new Llvm.Variable(name+Line.RANK); }
        public Llvm.Variable toLlvmType(int i ) { return new Llvm.Variable(name+i); }
    }
}
