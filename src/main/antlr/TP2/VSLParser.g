parser grammar VSLParser;

options {
  language = Java;
  tokenVocab = VSLLexer;
}

@header {
  package TP2;

  import java.util.stream.Collectors;
  import java.util.Arrays;
}


// TODO : other rules

program returns [ASD.Program out]
    : d=declFunction EOF { $out = new ASD.Program($d.out); }
    ;


declFunction returns[ArrayList<ASD.DeclFunction> out]
@init{
    ArrayList<ASD.DeclFunction> declFunctions = new ArrayList<ASD.DeclFunction>();
}
    : ((p=proto { declFunctions.add($p.out);})|(f=function {declFunctions.add($f.out);}) {$out = declFunctions;})+
    ;


proto returns [ASD.Proto out]
@init{
    ArrayList<SymbolTable.VariableSymbol> params = new ArrayList<SymbolTable.VariableSymbol>();
}
    : PROTO TYPE IDENT LP ((v=varsymb COMMA {params.add($v.out);})* v=varsymb {params.add($v.out);})? RP {$out = new ASD.Proto($TYPE.text, $IDENT.text, params);}
    ;

function returns [ASD.Function out]
@init{
    ArrayList<SymbolTable.VariableSymbol> params = new ArrayList<SymbolTable.VariableSymbol>();
}
    : FUNC TYPE IDENT LP ((v=varsymb COMMA {params.add($v.out);})* v=varsymb {params.add($v.out);})? RP i=instruction[$TYPE.text] {$out = new ASD.Function($TYPE.text, $IDENT.text, params, $i.out);}
    ;


instruction[String type] returns [ASD.Instruction out]
    : a=affectation { $out = $a.out; }
    | f=functionInstruction {$out = $f.out;}
    | e=expression { $out = $e.out; }
    | b=bloc[type] { $out = $b.out; }
    | d=declaration { $out = $d.out; }
    | w=iteration[type] {$out = $w.out;}
    | c=condition[type] {$out = $c.out;}
    | r=ret[type] {$out = $r.out; }
    | p=print {$out = $p.out;}
    | s=scan {$out = $s.out;}
    ;

condition [String type] returns [ASD.Condition out]
    : it=ifthen [type] {$out = $it.out; }
    | ite=ifthenelse [type] {$out = $ite.out; }
    ;

ifthen [String type] returns [ASD.IfThen out]
    : IF e=expression THEN i=instruction [type] FI {$out = new ASD.IfThen(new ASD.Comparison($e.out), $i.out);}
    ;

ifthenelse [String type] returns [ASD.IfThenElse out]
    : IF e=expression THEN i1=instruction [type] ELSE i2=instruction [type] FI {$out = new ASD.IfThenElse(new ASD.Comparison($e.out), $i1.out, $i2.out);}
    ;

bloc [String type] returns [ASD.Bloc out]
@init{
    ArrayList<ASD.Instruction> instructions = new ArrayList<ASD.Instruction>();
}
    : LB (i=instruction [type] {instructions.add($i.out);})* RB {$out = new ASD.Bloc(instructions); }
    ;

iteration [String type] returns [ASD.While out]
	: WHILE e=expression DO i=instruction [type] DONE {$out = new ASD.While(new ASD.Comparison($e.out), $i.out);}
	;

expression returns [ASD.Expression out]
    : l=expression PLUS r=factor {$out = new ASD.AddExpression($l.out, $r.out);}
    | l=expression MINUS r=factor {$out = new ASD.MinusExpression($l.out, $r.out);}
    | f=factor {$out = $f.out;}
    ;

factor returns [ASD.Expression out]
    : l=factor TIMES r=primary { $out = new ASD.TimesExpression($l.out, $r.out); }
    | l=factor DIVIDE r=primary { $out = new ASD.DivideExpression($l.out, $r.out); }
    | p=primary {$out = $p.out;}
    ;

primary returns [ASD.Expression out]
    : INTEGER { $out = new ASD.IntegerExpression($INTEGER.int); }
    | LP e = expression RP {$out = $e.out;}
    | v = variable {$out = new ASD.VariableExpression($variable.out);}
    | f = functionExpr {$out = $f.out;}
    ;
    //| variableValue {$out = $variable.out;}
    // TODO : that's all?
    //ELEMTAB -> tab[x]
  
print returns [ASD.Print out]
@init{
	String value = "";
	ArrayList<ASD.Expression> expressions = new ArrayList<ASD.Expression>();
}
	: PRINT ( (TEXT {value += $TEXT.text;} | e=expression {value += "%d"; expressions.add($e.out);}) COMMA)*
	  ( TEXT {value+= $TEXT.text;} | e=expression {value += "%d"; expressions.add($e.out);})
	  {$out = new ASD.Print(value, expressions);}
	;

scan returns [ASD.Scan out]
    : READ v=varsymb {$out = new ASD.Scan($v.out);}
    ;

affectation returns [ASD.Affectation out]
: v=variable AFFECT e=expression{$out = new ASD.Affectation($v.out, $e.out);}
;

declaration returns [ASD.Declaration out]
@init{
    ArrayList<ASD.DeclElem> declarations = new ArrayList<ASD.DeclElem>();
}
    : TYPE (d=declVariable {declarations.add($d.out);} COMMA )* (d2=declVariable {declarations.add($d2.out);}) {$out = new ASD.Declaration(new ASD.Int(), declarations);}
    ;

ret [String type] returns [ASD.Return out]
    : RETURN e=expression { $out = new ASD.Return($e.out, type); }
    ;

functionInstruction returns [ASD.FunctionInstruction out]
@init{
	ArrayList<ASD.Expression> params = new ArrayList<ASD.Expression>();
}
    : IDENT LP RP {$out = new ASD.FunctionInstruction($IDENT.text, new ArrayList<ASD.Expression>());}
    | IDENT LP (e=expression COMMA {params.add($e.out);})* (e=expression {params.add($e.out);} ) RP {$out = new ASD.FunctionInstruction($IDENT.text, params);}
    ;

functionExpr returns [ASD.FunctionExpression out]
@init{
	ArrayList<ASD.Expression> params = new ArrayList<ASD.Expression>();
}
    : IDENT LP RP {$out = new ASD.FunctionExpression($IDENT.text, new ArrayList<ASD.Expression>());}
    | IDENT LP (e=expression COMMA {params.add($e.out);})* (e=expression {params.add($e.out);} ) RP {$out = new ASD.FunctionExpression($IDENT.text, params);}
    ;


variable returns [ASD.Variable out]
    : IDENT { $out = new ASD.Variable(new ASD.IntVar(), $IDENT.text); } // variable
    // TODO : Tableaux
    ;

varsymb returns [SymbolTable.VariableSymbol out]
    : IDENT { $out = new SymbolTable.VariableSymbol(new ASD.Int(), $IDENT.text); }
    ;

declVariable returns [ASD.DeclVariable out]
    : IDENT {$out = new ASD.DeclVariable(new ASD.Variable(new ASD.Int(), $IDENT.text));}
    ;
