; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [9 x i8] c"1+3 = %d\00"


define i32 @plus(i32 %x, i32 %y){
%x1 = alloca i32 
store i32 %x, i32* %x1
%y2 = alloca i32 
store i32 %y, i32* %y2
%tmp1 = load i32, i32* %x1
%tmp2 = load i32, i32* %y2
%plus3 = add i32 %tmp1, %tmp2
ret i32 %plus3
ret i32 0
}

define void @main(){
%ecall4 = call i32 @plus(i32 1, i32 3)
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.fmt1, i32 0, i32 0), i32 %ecall4)
ret void
}


