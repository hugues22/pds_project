; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [24 x i8] c"%d est different de %d\0A\00"
@.fmt2 = global [18 x i8] c"%d est egal a %d\0A\00"


define void @compare(i32 %x, i32 %y){
%y2 = alloca i32 
store i32 %y, i32* %y2
%x1 = alloca i32 
store i32 %x, i32* %x1
%tmp1 = load i32, i32* %x1
%tmp2 = load i32, i32* %y2
%minus3 = sub i32 %tmp1, %tmp2
%icmp4 = icmp ne i32 %minus3, 0
br i1 %icmp4, label %then5, label %else6
then5:
%tmp3 = load i32, i32* %x1
%tmp4 = load i32, i32* %y2
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.fmt1, i32 0, i32 0), i32 %tmp3, i32 %tmp4)
br label %fi7
else6:
%tmp5 = load i32, i32* %x1
%tmp6 = load i32, i32* %y2
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.fmt2, i32 0, i32 0), i32 %tmp5, i32 %tmp6)
br label %fi7
fi7:
ret void
}

define void @main(){
call void @compare(i32 2, i32 1)
call void @compare(i32 1, i32 2)
call void @compare(i32 1, i32 1)
ret void
}


