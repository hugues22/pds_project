; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [12 x i8] c"%d+%d = %d\0A\00"
@.fmt2 = global [12 x i8] c"%d-%d = %d\0A\00"
@.fmt3 = global [12 x i8] c"%d*%d = %d\0A\00"
@.fmt4 = global [12 x i8] c"%d/%d = %d\0A\00"
@.fmt5 = global [12 x i8] c"%d+%d = %d\0A\00"
@.fmt6 = global [18 x i8] c"%d* (%d+%d) = %d\0A\00"
@.fmt7 = global [18 x i8] c"%d*  %d+%d  = %d\0A\00"


define void @expr(i32 %x, i32 %y){
%x0 = alloca i32 
store i32 %x, i32* %x0
%y0 = alloca i32 
store i32 %y, i32* %y0
%tmp1 = load i32, i32* %x0
%tmp2 = load i32, i32* %y0
%tmp3 = load i32, i32* %x0
%tmp4 = load i32, i32* %y0
%plus1 = add i32 %tmp3, %tmp4
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.fmt1, i32 0, i32 0), i32 %tmp1, i32 %tmp2, i32 %plus1)
%tmp5 = load i32, i32* %x0
%tmp6 = load i32, i32* %y0
%tmp7 = load i32, i32* %x0
%tmp8 = load i32, i32* %y0
%minus2 = sub i32 %tmp7, %tmp8
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.fmt2, i32 0, i32 0), i32 %tmp5, i32 %tmp6, i32 %minus2)
%tmp9 = load i32, i32* %x0
%tmp10 = load i32, i32* %y0
%tmp11 = load i32, i32* %x0
%tmp12 = load i32, i32* %y0
%times3 = mul i32 %tmp11, %tmp12
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.fmt3, i32 0, i32 0), i32 %tmp9, i32 %tmp10, i32 %times3)
%tmp13 = load i32, i32* %x0
%tmp14 = load i32, i32* %y0
%tmp15 = load i32, i32* %x0
%tmp16 = load i32, i32* %y0
%divide4 = udiv i32 %tmp15, %tmp16
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.fmt4, i32 0, i32 0), i32 %tmp13, i32 %tmp14, i32 %divide4)
%tmp17 = load i32, i32* %x0
%tmp18 = load i32, i32* %x0
%plus5 = add i32 %tmp18, 1
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.fmt5, i32 0, i32 0), i32 %tmp17, i32 1, i32 %plus5)
%tmp19 = load i32, i32* %x0
%tmp20 = load i32, i32* %x0
%tmp21 = load i32, i32* %y0
%tmp22 = load i32, i32* %x0
%tmp23 = load i32, i32* %x0
%tmp24 = load i32, i32* %y0
%plus6 = add i32 %tmp23, %tmp24
%times7 = mul i32 %tmp22, %plus6
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.fmt6, i32 0, i32 0), i32 %tmp19, i32 %tmp20, i32 %tmp21, i32 %times7)
%tmp25 = load i32, i32* %x0
%tmp26 = load i32, i32* %x0
%tmp27 = load i32, i32* %y0
%tmp28 = load i32, i32* %x0
%tmp29 = load i32, i32* %x0
%times8 = mul i32 %tmp28, %tmp29
%tmp30 = load i32, i32* %y0
%plus9 = add i32 %times8, %tmp30
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.fmt7, i32 0, i32 0), i32 %tmp25, i32 %tmp26, i32 %tmp27, i32 %plus9)
ret void
}

define void @main(){
call void @expr(i32 1, i32 3)
call void @expr(i32 5, i32 2)
ret void
}


