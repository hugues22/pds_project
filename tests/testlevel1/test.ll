; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [27 x i8] c"Input n between 0 and 11:\0A\00"
@.fmt2 = global [3 x i8] c"%d\00"
@.fmt3 = global [12 x i8] c"f(%d) = %d\0A\00"


define i32 @test(i32 %i){
%i = alloca i32 
%tmp1 = load i32* %i
%times1 = times i32 %tmp1, 3
ret i32 %times1
}

define i32 @main(i32 %i, i32 %m){
%i = alloca i32 
%m = alloca i32 
%i = alloca i32 
%j = alloca i32 
store i32 10, i32* %k
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.fmt1, i64 0, i64 0))
call i32 (i8*, ... ) @scanf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt2, i64 0, i64 0), i32* %m)
call i32 (i8*, ... ) @scanf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt2, i64 0, i64 0), i32* %i)
br label %while5
while5:
%tmp2 = load i32* %i
%tmp3 = load i32* %j
%minus2 = minus i32 %tmp2, %tmp3
%icmp3 = icmp ne i32 %minus2, 0
br i1 %icmp3, label %do6, label %done7
do6:
%tmp4 = load i32* %i
%plus4 = add i32 %tmp4, 1
store i32 %plus4, i32* %i
br label %while5
done7:
%tmp5 = load i32* %i
%tmp6 = load i32* %j
%plus8 = add i32 %tmp5, %tmp6
%icmp9 = icmp ne i32 %plus8, 0
br i1 %icmp9, label %then11, label %fi12
then11:
%tmp7 = load i32* %i
%ecall10 = call i32 @test(i32 %tmp7)
ret i32 %ecall10
br label %fi12
fi12:
%tmp8 = load i32* %i
%times13 = times i32 4, 3
%plus14 = add i32 %tmp8, %times13
%tmp9 = load i32* %i
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.fmt3, i64 0, i64 0), i32 %plus14, i32 %tmp9)
ret i32 1
}


