; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)



define i32 @main(){
br label %while2
while2:
%icmp1 = icmp ne i32 1, 0
br i1 %icmp1, label %do3, label %done4
do3:
ret i32 0
br label %while2
}


