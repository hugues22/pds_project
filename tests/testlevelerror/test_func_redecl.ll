; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [2 x i8] c"a\00"
@.fmt2 = global [2 x i8] c"a\00"


define i32 @main(){
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.fmt1, i32 0, i32 0))
ret i32 0
ret i32 0
}

define i32 @main(){
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.fmt2, i32 0, i32 0))
ret i32 0
ret i32 0
}


