; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)



define i32 @f(){
%t1 = alloca i32 
%tmp1 = load i32, i32* %t1
%tmp2 = load i32, i32* %t1
ret i32 %tmp2
ret i32 0
}

define i32 @main(){
%i1 = alloca i32 
%ecall1 = call i32 @f()
store i32 %ecall1, i32* %i1
ret i32 0
ret i32 0
}


