; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [2 x i8] c"\0A\00"
@.fmt2 = global [8 x i8] c"%d^2 + \00"
@.fmt3 = global [10 x i8] c"1^2 = %d\0A\00"


define void @main(){
%n1 = alloca i32 
%i1 = alloca i32 
%s1 = alloca i32 
store i32 5, i32* %n1
store i32 0, i32* %s1
%tmp1 = load i32, i32* %n1
store i32 %tmp1, i32* %i1
br label %while5
while5:
%tmp2 = load i32, i32* %i1
%icmp1 = icmp ne i32 %tmp2, 0
br i1 %icmp1, label %do6, label %done7
do6:
%tmp3 = load i32, i32* %s1
%tmp4 = load i32, i32* %i1
%tmp5 = load i32, i32* %i1
%times2 = mul i32 %tmp4, %tmp5
%plus3 = add i32 %tmp3, %times2
store i32 %plus3, i32* %s1
%tmp6 = load i32, i32* %i1
%minus4 = sub i32 %tmp6, 1
store i32 %minus4, i32* %i1
br label %while5
done7:
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.fmt1, i32 0, i32 0))
%tmp7 = load i32, i32* %n1
store i32 %tmp7, i32* %i1
br label %while11
while11:
%tmp8 = load i32, i32* %i1
%minus8 = sub i32 %tmp8, 1
%icmp9 = icmp ne i32 %minus8, 0
br i1 %icmp9, label %do12, label %done13
do12:
%tmp9 = load i32, i32* %i1
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.fmt2, i32 0, i32 0))
%tmp10 = load i32, i32* %i1
%minus10 = sub i32 %tmp10, 1
store i32 %minus10, i32* %i1
br label %while11
done13:
%tmp11 = load i32, i32* %s1
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.fmt3, i32 0, i32 0))
ret void
}


