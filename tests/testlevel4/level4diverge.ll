; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [5 x i8] c"Toto\00"


define void @main(){
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.fmt1, i32 0, i32 0))
br label %while2
while2:
%icmp1 = icmp ne i32 1, 0
br i1 %icmp1, label %do3, label %done4
do3:
%x2 = alloca i32 
store i32 1, i32* %x2
br label %while2
done4:
ret void
}


