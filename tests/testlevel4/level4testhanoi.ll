; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [31 x i8] c"Deplacer un disque de %d a %d\0A\00"
@.fmt2 = global [26 x i8] c"\0A Hanoi avec %d disques\0A\0A\00"
@.fmt3 = global [26 x i8] c"\0A\0AHanoi avec %d disques\0A\0A\00"


define i32 @hanoi(i32 %n, i32 %delatour, i32 %alatour, i32 %parlatour){
%n0 = alloca i32 
store i32 %n, i32* %n0
%delatour0 = alloca i32 
store i32 %delatour, i32* %delatour0
%alatour0 = alloca i32 
store i32 %alatour, i32* %alatour0
%parlatour0 = alloca i32 
store i32 %parlatour, i32* %parlatour0
%b1 = alloca i32 
%tmp1 = load i32, i32* %n0
%icmp1 = icmp ne i32 %tmp1, 0
br i1 %icmp1, label %then6, label %fi7
then6:
%tmp2 = load i32, i32* %n0
%minus2 = sub i32 %tmp2, 1
%tmp3 = load i32, i32* %delatour0
%tmp4 = load i32, i32* %parlatour0
%tmp5 = load i32, i32* %alatour0
%ecall3 = call i32 @hanoi(i32 %minus2, i32 %tmp3, i32 %tmp4, i32 %tmp5)
store i32 %ecall3, i32* %b1
%tmp6 = load i32, i32* %delatour0
%tmp7 = load i32, i32* %alatour0
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.fmt1, i32 0, i32 0), i32 %tmp6, i32 %tmp7)
%tmp8 = load i32, i32* %n0
%minus4 = sub i32 %tmp8, 1
%tmp9 = load i32, i32* %parlatour0
%tmp10 = load i32, i32* %alatour0
%tmp11 = load i32, i32* %delatour0
%ecall5 = call i32 @hanoi(i32 %minus4, i32 %tmp9, i32 %tmp10, i32 %tmp11)
store i32 %ecall5, i32* %b1
br label %fi7
fi7:
ret i32 1
ret i32 0
}

define void @main(){
%ndisque1 = alloca i32 
%a1 = alloca i32 
store i32 3, i32* %ndisque1
%tmp12 = load i32, i32* %ndisque1
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.fmt2, i32 0, i32 0))
%tmp13 = load i32, i32* %ndisque1
%ecall8 = call i32 @hanoi(i32 %tmp13, i32 1, i32 3, i32 2)
store i32 %ecall8, i32* %a1
store i32 4, i32* %ndisque1
%tmp14 = load i32, i32* %ndisque1
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.fmt3, i32 0, i32 0))
%tmp15 = load i32, i32* %ndisque1
%ecall9 = call i32 @hanoi(i32 %tmp15, i32 1, i32 3, i32 2)
store i32 %ecall9, i32* %a1
ret void
}


