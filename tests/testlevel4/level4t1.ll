; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [19 x i8] c"Et voila: %d%d%d%d\00"


define void @main(){
%i1 = alloca i32 
%j1 = alloca i32 
%k1 = alloca i32 
%l1 = alloca i32 
store i32 0, i32* %i1
store i32 0, i32* %j1
store i32 0, i32* %k1
store i32 0, i32* %l1
%i2 = alloca i32 
%j2 = alloca i32 
%k2 = alloca i32 
store i32 1, i32* %l1
%i3 = alloca i32 
%j3 = alloca i32 
store i32 2, i32* %k2
%i4 = alloca i32 
store i32 3, i32* %j3
%tmp1 = load i32, i32* %i1
%tmp2 = load i32, i32* %j1
%tmp3 = load i32, i32* %k1
%tmp4 = load i32, i32* %l1
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.fmt1, i32 0, i32 0), i32 %tmp1, i32 %tmp2, i32 %tmp3, i32 %tmp4)
ret void
}


