; Target
target triple = "x86_64-unknown-linux-gnu"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)

@.fmt1 = global [3 x i8] c"%d\00"


define void @main(i32 %f){
%f0 = alloca i32 
store i32 %f, i32* %f0
%f1 = alloca i32 
store i32 100, i32* %f1
%tmp1 = load i32, i32* %f1
call i32 (i8*, ... ) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt1, i32 0, i32 0))
ret void
}


